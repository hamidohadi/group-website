---
title: Collaborators
summary: Links to internal and external groups
date: "2018-06-28T00:00:00Z"

reading_time: false  # Show estimated reading time?
share: false  # Show social sharing links?
profile: false  # Show author profile?
comments: false  # Show comments?

# Optional header image (relative to `static/media/` folder).
header:
  caption: ""
  image: ""
---



| External   | Internal   |
|---|---|
| [Dr Thomas Volz](https://researchers.mq.edu.au/en/persons/thomas-volz) (Macquarie)  | [Prof Andrea Di Falco](http://synthopt.wp.st-andrews.ac.uk/) |
| [Prof Jeremy Baumberg](http://np.phy.cam.ac.uk) (Cambridge) | [Prof Kishan Dholakia](http://opticalmanipulationgroup.wp.st-andrews.ac.uk/) |
| [Prof Pavlos Savvidis](http://quantopt.materials.uoc.gr/) (Crete) | [Profs Ifor Samuel & Graham Turnbull](https://www.st-andrews.ac.uk/physics/osc/home.shtml) |
| [Dr Andrew Ramsay](http://www.hit.phy.cam.ac.uk/Members/Ramsay/) (Hitachi) | [Dr Jonathan Keeling](https://www.st-andrews.ac.uk/~jmjk/) |
| [Dr Thomas Pohl](https://pure.au.dk/portal/en/persons/thomas-pohl(ffdd61b1-591b-445b-95ef-3898135a3e76).html) (Aarhus) | [Dr Julia Payne](https://jlpgroup.wp.st-andrews.ac.uk)  |
| [Prof Matthew Jones](https://www.dur.ac.uk/qlm/members/?id=4705) (Durham)| |
| [Dr  Hadiseh Alaeian](https://engineering.purdue.edu/qnp) (Purdue) |  |


# Funding
[![alt text](/media/funders/ukri.png "EPSRC")](https://epsrc.ukri.org)
[![alt text](/media/funders/Leverhulme.png "Leverhulme Trust")](https://www.leverhulme.ac.uk)
[![alt text](/media/funders/RS.png "The Royal Society")](https://royalsociety.org)
[![alt text](/media/funders/carnegie.png "The Carnegie Trust")](https://www.carnegie-trust.org)
