---
title: PhD position for 2022 academic year
date: 2021-12-15T10:41:31.666Z
summary: We have one fully funded (covering tuition fees and living expenses)
  PhD position for 2022 academic year on **perovskite** **polairtons**. This
  position is **open to all nationals**. Interested candidates should send their
  CV to [Dr Ohadi](mailto:ho35@st-andrews.ac.uk) before [formally
  applying](https://www.st-andrews.ac.uk/study/fees-and-funding/postgraduate/scholarships/world-leading-10/).
draft: false
featured: false
image:
  filename: featured
  focal_point: Smart
  preview_only: false
---
We have one fully funded (covering tuition fees and living expenses) PhD position for 2022 academic year on **perovskite** **polaritons**. This position is **open to all nationals**. Interested candidates should send their CV to [Dr Ohadi](mailto:ho35@st-andrews.ac.uk) before [formally applying](https://www.st-andrews.ac.uk/study/fees-and-funding/postgraduate/scholarships/world-leading-10/).
