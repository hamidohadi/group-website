---
title: PhD position for 2023
date: 2022-07-29T10:41:31.666Z
summary: We have one fully funded (covering tuition fees and living expenses)
  PhD position to start in Jan 2023 on **perovskite** **polairtons**. This
  position is **open to all nationals**. Interested candidates should send their
  CV to [Dr Ohadi](mailto:ho35@st-andrews.ac.uk).
draft: false
featured: false
image:
  filename: featured
  focal_point: Smart
  preview_only: false
---
We have one fully funded (covering tuition fees and living expenses) PhD position for Jan 2023 on **perovskite** **polaritons**. This position is **open to all nationals**. Interested candidates should send their CV to [Dr Ohadi](mailto:ho35@st-andrews.ac.uk).
