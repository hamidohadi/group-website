---
title: Research fellow position
subtitle: 
# Summary for listings and search engines
summary: We have a postdoc position availabe for our [Rydberg Polaritons]({{< ref "project/rydberg-polaritons.md" >}}) project. Interested candidates can apply formally [here](https://www.vacancies.st-andrews.ac.uk/Vacancies/W/2058/0/225473/889/research-fellow-in-strong-light-matter-interaction-in-cuprous-oxide-microcavities-ar2181rmr). Informal inquiries can be sent to [Dr Ohadi](mailto:ho35@st-andrews.ac.uk).


# Link this post with a project
projects: []

# Date published
date: "2019-03-04T00:00:00Z"

# Date updated
lastmod: "2019-03-04T00:00:00Z"

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
# image:
#   caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/CpkOjOcXdUY)'
#   focal_point: ""
#   placement: 2
#   preview_only: false

authors:
- admin

tags:
- Postdoc positions

categories:
- Jobs
---

We have a postdoc position availabe for our [Rydberg Polaritons]({{< ref "project/rydberg-polaritons.md" >}}) project. Interested candidates can apply formally [here](https://www.vacancies.st-andrews.ac.uk/Vacancies/W/2058/0/225473/889/research-fellow-in-strong-light-matter-interaction-in-cuprous-oxide-microcavities-ar2181rmr). Informal inquiries can be sent to [Dr Ohadi](mailto:ho35@st-andrews.ac.uk).


Applications are invited for a postdoctoral position working on Rydberg polaritons in Cu2O microcavities at the school of Physics and Astronomy at the University of St Andrews. 

In this project, we aim to hybridise photons with Rydberg excitons. Rydberg excitons are highly excited electron-hole pairs that can span macroscopic dimensions. Because of their macroscopic dimensions they strongly repel each other. The semiconductor device that we have chosen for hybridisation is a 2-dimensional semiconductor microcavity formed by two highly reflective mirrors encompassing nanocrystals and thin films of cuprous oxide. Photons confined in the microcavity strongly couple to Rydberg excitons in cuprous oxide to form Rydberg polaritons. The Rydberg polaritons interaction strength will be orders of magnitude higher than the current microcavity polaritons. This breakthrough will allow us to explore quantum optics at the single-particle limit and form 2-dimensional networks of strongly correlated photons for future single-photon switches and quantum simulators.

This project is part of a UK funded Grant by EPSRC to develop the emergent field of Rydberg polaritonics. By hybridising photons and Rydberg excitons using strong coupling, we will produce a new generation of microcavity polaritons which have orders of magnitude larger nonlinearities than conventional semiconductor microcavities.

We are seeking a researcher with expertise in the fabrication and spectroscopy of semiconductor materials. The successful candidate will design novel nanophotonic structures, fabricate these and measure their photonic properties. The candidate will have access to the extensive fabrication and characterisation facilities at the School of Physics and Astronomy, and will work in collaboration with partner universities (Heriot-Watt in UK and National Tsing Hua University in Taiwan) and Cambridge Hitachi Laboratory. A PhD in a physical science is required and prior experience in spectroscopy is essential. Experience in fabrication, strong light-matter coupling and cryogenics is desirable.
