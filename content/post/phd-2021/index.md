---
title: PhD position for 2021 academic year
subtitle: 
# Summary for listings and search engines
summary: We have one fully funded (covering tuition fees and living expenses) PhD position for 2021 academic year. This position is open to all nationals. 

# Link this post with a project
projects: []

# Date published
date: "2020-12-01T00:00:00Z"

# Date updated
lastmod: "2020-12-01T00:00:00Z"

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
# image:
#   caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/CpkOjOcXdUY)'
#   focal_point: ""
#   placement: 2
#   preview_only: false

# authors:
# - admin

tags:
- PhD positions

categories:
- Jobs
---

We have one fully funded (covering tuition fees and living expenses) PhD position for 2021
academic year. This position is **open to all nationals**. Interested candidates should send their CV to [Dr Ohadi](mailto:ho35@st-andrews.ac.uk) before [formally applying](https://www.st-andrews.ac.uk/physics-astronomy/prospective/pgr/).

## Project description
### Light matter coupling of quantum emitters in two-dimensional materials
Defects in two-dimensional materials have recently attracted a lot of interest as they have been shown to have quantum features like single atoms: they have well-defined energy levels, and once excited they can emit one photon at a time. These characteristics are crucial for quantum technologies such as quantum memories and single-photon sources. Coupling the emission from these defects to photonic cavities allows mapping their quantum states to photons which can then be transported and stored, as well as using them as high brightness single-photon sources.

In this project, we are aiming to use carbon defects in hexagonal boron nitride layers as quantum emitters. You will fabricate single-photon sources by placing these defects inside high quality optical Fabry-Perot cavities, and couple their emission to optical fibers. You will study the quantum operation of the device by mapping the photon statistics of the coupled light. 

Reference:  Koperski, M. et al. "Midgap radiative centers in carbon-enriched hexagonal boron nitride" [PNAS 117, 13214 (2020)](https://www.pnas.org/content/pnas/117/24/13214.full.pdf)
