---
title: PhD position for 2020 academic year
subtitle: 
# Summary for listings and search engines
summary: We have one fully funded (covering tuition fees and living expenses) PhD position for 2020

# Link this post with a project
projects: []

# Date published
date: "2019-12-13T00:00:00Z"

# Date updated
lastmod: "2019-12-13T00:00:00Z"

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
# image:
#   caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/CpkOjOcXdUY)'
#   focal_point: ""
#   placement: 2
#   preview_only: false

authors:
- admin

tags:
- PhD positions

categories:
- Jobs
---


We have one fully funded (covering tuition fees and living expenses) PhD position for 2020
academic year in between University of St Andrews and Macquarie University in Sydney, Australia in [Strong light-matter coupling with Rydberg polaritons]({{< ref "project/rydberg-polaritons.md" >}}).

This position is **open to all nationals**. For more information please see [the post ad](/~qflight/files/ads/StAnd-Macquarie.pdf).

Interested candidates should send their CV to [Dr Ohadi](mailto:ho35@st-andrews.ac.uk) before [formally applying](https://www.st-andrews.ac.uk/physics/prosp_pg/phd/index.php).
