---
# Display name
title: Anindya Paul

# Is this the primary user of the site?
superuser: false

user_groups: ["PhD Students"]

# Role/position/tagline
role: Postgraduate Student

# Organizations/Affiliations to show in About widget
# organizations:
# - name: Stanford University
#   url: https://www.stanford.edu/

# Short bio (displayed in user profile at end of posts)
bio: 

# Interests to show in About widget
interests:
- Nanophotonics
- Spectroscopy
- Rydberg polaritons
- Cold atoms


# Education to show in About widget
education:
  courses:
  - course: Masters in Physics
    institution: NIT Rourkela
    year: 2019
  - course: BSc in Physics
    institution: Assam University
    year: 2017


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:asp23@st-andrews.ac.uk'
# - icon: google-scholar
#   icon_pack: ai
#   link: https://scholar.google.com/citations?hl=en&user=mAztOK8AAAAJ
# - icon: orcid
#   icon_pack: ai
#   link: https://orcid.org/0000-0002-9079-120X
# - icon: graduation-cap  # Alternatively, use `google-scholar` icon from `ai` icon pack
#   icon_pack: fas
#   link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
# - icon: github
#   icon_pack: fab
#   link: https://github.com/gcushen
# - icon: linkedin
#   icon_pack: fab
#   link: https://www.linkedin.com/

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "asp23@st-andrews.ac.uk"

# Highlight the author in author lists? (true/false)
highlight_name: false
---
Anindya is a joint QM-CDT PhD student between our group and Prof Thomas Volz at Macquarie in
Australia. Aninyda is working on strong coupling of Rydberg excitons to cavity photons.
