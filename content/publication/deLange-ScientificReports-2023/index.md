---
# Documentation: https://wowchemy.com/docs/managing-content/
title: Highly-excited Rydberg excitons in synthetic thin-film cuprous oxide
publication_types:
  - "2"
authors:
  - J. DeLange
  - K. Barua
  - A. S. Paul
  - H. Ohadi
  - V. Zwiller
  - S. Steinhauer
  - H. Alaeian
doi: 10.1038/s41598-023-41465-y
publication: "Scientific Reports"
publication_short: "Sci Rep"
# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
  - rydberg-polaritons
abstract: Cuprous oxide ($\hbox {Cu}{}_2\hbox {O}$) has recently emerged as a promising material in solid-state quantum technology, specifically for its excitonic Rydberg states characterized by large principal quantum numbers (n). The significant wavefunction size of these highly-excited states (proportional to $n^2$) enables strong long-range dipole-dipole (proportional to $n^4$) and van der Waals interactions (proportional to $n^{11}$). Currently, the highest-lying Rydberg states are found in naturally occurring $\hbox {Cu}_2\hbox {O}$. However, for technological applications, the ability to grow high-quality synthetic samples is essential. The fabrication of thin-film $\hbox {Cu}{}_2\hbox {O}$samples is of particular interest as they hold potential for observing extreme single-photon nonlinearities through the Rydberg blockade. Nevertheless, due to the susceptibility of high-lying states to charged impurities, growing synthetic samples of sufficient quality poses a substantial challenge. This study successfully demonstrates the CMOS-compatible synthesis of a $\hbox {Cu}{}_2\hbox {O}$thin film on a transparent substrate that showcases Rydberg excitons up to $n = 8$which is readily suitable for photonic device fabrications. These findings mark a significant advancement towards the realization of scalable and on-chip integrable Rydberg quantum technologies.
summary: Synthetic cuprous oxide films have been grown and shown to host Rydberg excitons.
draft: false
featured: false
tags:
  - polariton
  - Cu2O
image:
  filename: thumb.png
  focal_point: Smart
  preview_only: false
date: 2023-09-10T11:27:44.188Z
---