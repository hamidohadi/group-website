---
# Documentation: https://wowchemy.com/docs/managing-content/

title: Robust platform for engineering pure-quantum-state transitions in polariton
  condensates
subtitle: ''
summary: ''
authors:
- A. Askitopoulos
- T. C. H. Liew
- H. Ohadi
- Z. Hatzopoulos
- P. G. Savvidis
- P. G. Lagoudakis
tags: []
categories: []
date: '2015-07-01'
lastmod: 2020-12-28T15:15:06Z
featured: false
draft: false

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ''
  focal_point: ''
  preview_only: false

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
publishDate: '2020-12-31T22:03:53.600227Z'
publication_types:
- '2'
abstract: We report on pure-quantum-state polariton condensates in optical annular
  traps. The study of the underlying mechanism reveals that the polariton wave function
  always coalesces in a single pure quantum state that, counterintuitively, is always
  the uppermost confined state with the highest overlap with the exciton reservoir.
  The tunability of such states combined with the short polariton lifetime allows
  for ultrafast transitions between coherent mesoscopic wave functions of distinctly
  different symmetries, rendering optically confined polariton condensates a promising
  platform for applications such as many-body quantum circuitry and continuous-variable
  quantum processing.
publication: '*Physical Review B*'
url_pdf: http://link.aps.org/doi/10.1103/PhysRevB.92.035305
doi: 10.1103/PhysRevB.92.035305
---
