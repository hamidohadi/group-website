---
# Documentation: https://wowchemy.com/docs/managing-content/
title: Nonlinear Rydberg Exciton-Polaritons in Cu2O Microcavities
publication_types:
  - "2"
authors:
  - Maxim Makhonin
  - Anthonin Delphan
  - Kok Wee Song
  - Paul Walker
  - Tommi Isoniemi
  - Peter Claronino
  - Konstantinos Orfanakis
  - Sai Kiran Rajendran
  - Hamid Ohadi
  - Julian Heckötter
  - Marc Assmann
  - Manfred Bayer
  - Alexander Tartakovskii
  - Maurice Skolnick
  - Oleksandr Kyriienko
  - Dmitry Krizhanovskii

doi: 10.1038/s41377-024-01382-9
publication: "Light Science and Applications"
publication_short: "Light. Sci. Appl."
# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
  - rydberg-polaritons
abstract: Rydberg excitons (analogues of Rydberg atoms in condensed matter systems) are highly excited bound electron-hole states with large Bohr radii. The interaction between them as well as exciton coupling to light may lead to strong optical nonlinearity, with applications in sensing and quantum information processing. Here, we achieve strong effective photon–photon interactions (Kerr-like optical nonlinearity) via the Rydberg blockade phenomenon and the hybridisation of excitons and photons forming polaritons in a Cu2O-filled microresonator. Under pulsed resonant excitation polariton resonance frequencies are renormalised due to the reduction of the photon-exciton coupling with increasing exciton density. Theoretical analysis shows that the Rydberg blockade plays a major role in the experimentally observed scaling of the polariton nonlinearity coefficient as ∝ n^4.4±1.8 for principal quantum numbers up to n=7. Such high principal quantum numbers studied in a polariton system for the first time are essential for realisation of high Rydberg optical nonlinearities, which paves the way towards quantum optical applications and fundamental studies of strongly correlated photonic (polaritonic) states in a solid state system.
summary: Observation of a nonlinearity in Rydberg polaritons in Cu2O.
draft: false
featured: false
tags:
  - polariton
image:
  filename: thumb.png
  focal_point: Smart
  preview_only: false
date: 2024-01-06T11:27:44.188Z
---