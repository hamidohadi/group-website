---
# Documentation: https://wowchemy.com/docs/managing-content/
title: Local tuning of Rydberg exciton energies in nanofabricated Cu2O pillars
publication_types:
  - "2"
authors:
  - Anindya Sundar Paul
  - Sai Kiran Rajendran
  - David Ziemkiewicz
  - Thomas Volz
  - Hamid Ohaid

doi: 10.1038/s43246-024-00481-9
publication: "Communications Materials"
publication_short: "Commun. Mater."
# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
  - rydberg-polaritons
abstract: Rydberg excitons in Cu2O feature giant optical nonlinearities. To exploit these nonlinearities for quantum applications, the confinement must match the Rydberg blockade size, which in Cu2O could be as large as a few microns. Here, in a top-down approach, we show how exciton confinement can be realised by focused-ion-beam etching of a polished bulk Cu2O crystal without noticeable degradation of the excitonic properties. The etching of the crystal to micron sizes allows for tuning the energies of Rydberg excitons locally, and precisely, by optically induced temperature change. These results pave the way for exploiting the large nonlinearities of Rydberg excitons in micropillars for making non-classical light sources, while the precise tuning of their emission energy opens up a viable pathway for realising a scalable photonic quantum simulation platform.
summary: Rydberg excitons in cuprous oxide feature giant optical nonlinearities that may be exploited in quantum applications if suitably confined. Here, the authors show how exciton confinement can be realised by focused-ion-beam etching of Cu2O crystals without noticeable degradation of excitonic properties.
draft: false
featured: false
tags:
  - polariton
image:
  filename: thumb.png
  focal_point: Smart
  preview_only: false
date: 2024-03-28T13:26:10.796Z
---