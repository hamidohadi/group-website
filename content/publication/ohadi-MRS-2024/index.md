---
# Documentation: https://wowchemy.com/docs/managing-content/
title: "Materials for excitons–polaritons: Exploiting the diversity of semiconductors"
publication_types:
  - "2"
authors:
  - J. Bellessa
  - J. Bloch
  - E. Deleporte
  - V. M. Menon
  - H. S. Nguyen
  - H. Ohadi
  - S. Ravet
  - T. Boulier
doi: 10.1557/s43577-024-00779-6
publication: "MRS Bulletin"
publication_short: "MRS Bulletin"
# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
  - rydberg-polaritons
  - room-T-polaritons
abstract: The regime of strong coupling between photons and excitons gives rise to hybrid light–matter particles with fascinating properties and powerful implications for semiconductor quantum technologies. As the properties of excitons crucially depend on their host crystal, a rich field of exciton–polariton engineering opens by exploiting the diversity of semiconductors currently available. From dimensionality to binding energy to unusual orbitals, various materials provide different fundamental exciton properties that are often complementary, enabling vast engineering possibilities. This article aims to showcase some of the main materials for strong light–matter engineering, focusing on their fundamental complementarity and what this entails for future quantum technologies.
summary: Review paper on new perspectices on materials for exciton-polritons
draft: false
featured: false
tags:
  - polariton
image:
  filename: thumb.png
  focal_point: Smart
  preview_only: false
date: 2024-08-30T11:27:44.188Z
---