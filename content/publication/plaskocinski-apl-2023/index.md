---
# Documentation: https://wowchemy.com/docs/managing-content/
title: Laser writing of parabolic micromirrors with a high numerical aperture for optical trapping and rotation
publication_types:
  - "2"
authors:
  - T. Plaskocinski
  - Y. Arita
  - G. D. Bruce
  - S. Persheyev
  - K. Dholakia
  - A. Di Falco
  - H. Ohadi
doi: 10.1063/5.0155512
publication: "Applied Physics Letters"
publication_short: "APL"
# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
  - lattices
  - room-T-polaritons
abstract: TOn-chip optical trapping systems allow for high scalability and lower the barrier to access. Systems capable of trapping multiple particles typically come with high cost and complexity. Here, we present a technique for making parabolic mirrors with micrometer-size dimensions and high numerical apertures (NA &gt; 1). Over 350 mirrors are made by simple CO2 laser ablation of glass followed by gold deposition. We fabricate mirrors of arbitrary diameter and depth at a high throughput rate by carefully controlling the ablation parameters. We use the micromirrors for three-dimensional optical trapping of microbeads in solution, achieving a maximum optical trap stiffness of 52 pN/μm/W. We, then, further demonstrate the viability of the mirrors as in situ optical elements through the rotation of a vaterite particle using reflected circularly polarized light. The method used allows for rapid and highly customizable fabrication of dense optical arrays.
summary: Laser ablation of micron-size parabolic mirrors on glass, and using it for trapping of particles
draft: false
featured: false
tags:
  - polariton
image:
  filename: thumb.png
  focal_point: Smart
  preview_only: false
date: 2023-09-05T11:27:44.188Z
---